import { MatSnackBar } from '@angular/material';
import { Inject, Injectable } from '@angular/core';
import { MatSnackBarConfig } from '@angular/material/snack-bar/typings/snack-bar-config';

@Injectable({
  providedIn: 'root'
})
export default class Notifications {
  constructor(@Inject('SnackBar') private snackBar: MatSnackBar) {}

  notify(message: string, action: string, config: MatSnackBarConfig = { duration: 2000 }) {
    this.snackBar.open(message, action, config);
  }

  success(message: string, config: MatSnackBarConfig = { duration: 2000 }) {
    this.snackBar.open(message, 'Success!', config);
  }
}
