import { MatSnackBar } from '@angular/material';
import { Inject, Injectable } from '@angular/core';
import { MatSnackBarConfig } from '@angular/material/snack-bar/typings/snack-bar-config';
import Notifications from '@app/notifications/notifications';

export default class MockNotifications extends Notifications {
  private messages: { message: string; action: string }[];

  constructor() {
    super(null);
    this.messages = [];
  }

  notify(message: string, action: string, config: MatSnackBarConfig = { duration: 2000 }) {
    this.messages.push({ message: message, action: action });
  }

  success(message: string, config: MatSnackBarConfig = { duration: 2000 }) {
    this.messages.push({ message: message, action: 'success!' });
  }
}
