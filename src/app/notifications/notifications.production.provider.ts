import { MatSnackBar } from '@angular/material';
import Notifications from '@app/notifications/notifications';

export let NotificationsProvider = [
  { provide: 'SnackBar', useExisting: MatSnackBar },
  { provide: 'Notifications', useClass: Notifications }
];
