import MockNotifications from '@app/notifications/mock-notifications';

export let NotificationsProvider = [{ provide: 'Notifications', useValue: new MockNotifications() }];
