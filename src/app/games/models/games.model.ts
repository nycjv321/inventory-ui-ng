import Game from '@app/games/models/game.model';

export default interface Games {
  // The games's category: 'dev', 'explicit'...
  games: Game[];
}
