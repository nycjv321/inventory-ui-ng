export default interface Genre {
  name: string;
  id: string;
}
