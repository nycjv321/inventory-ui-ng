class EmptyGame implements Game {
  description: string;
  genre: string;
  id: number;
  release_date: string;
  system: string;
  title: string;

  constructor() {
    this.description = '';
    this.genre = '';
    this.id = -1;
    this.release_date = '';
    this.system = '';
    this.title = '';
  }
}

function createEmptyGame(): Game {
  return new EmptyGame();
}

export default interface Game {
  id: number;
  title: string;
  description: string;
  release_date: string;
  genre: string;
  system: string;
}

function formattedDate(date: Date): string {
  return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
}

export { Game, createEmptyGame, formattedDate };
