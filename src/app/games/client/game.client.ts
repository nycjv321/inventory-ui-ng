import { Observable } from 'rxjs';
import Games from '@app/games/models/games.model';
import Game from '@app/games/models/game.model';
import Genre from '@app/games/models/genre.model';
import System from '@app/games/models/system.model';
import { HttpErrorResponse } from '@angular/common/http';

export default interface GameClient {
  getAll(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Games>;
  delete(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<any>;
  getGenres(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Genre[]>;
  getSystems(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<System[]>;
  create(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game>;
  update(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game>;
}
