import GameClient from '@app/games/client/game.client';
import Game, { createEmptyGame } from '@app/games/models/game.model';
import { Observable } from 'rxjs';
import Games from '@app/games/models/games.model';
import Genre from '@app/games/models/genre.model';
import System from '@app/games/models/system.model';
import { HttpErrorResponse } from '@angular/common/http';

export default class MockGameClient implements GameClient {
  private games: Games;
  constructor() {
    this.games = { games: [] };
  }
  create(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game> {
    return new Observable(subscriber => {
      this.games.games.push(game);
      subscriber.next(game);
    });
  }

  delete(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<any> {
    const games = this.games.games;
    for (let i = 0; i < games.length; i++) {
      if (games[i].id === game.id) {
        games.splice(i, 1);
      }
    }
    return new Observable();
  }

  getAll(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Games> {
    return new Observable(subscriber => {
      subscriber.next(this.games);
    });
  }

  getGenres(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Genre[]> {
    return new Observable(subscriber => {
      subscriber.next([{ id: '0', name: 'Platform' }]);
    });
  }

  getSystems(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<System[]> {
    return new Observable(subscriber => {
      subscriber.next([{ id: '0', name: 'Nintendo Switch' }]);
    });
  }

  update(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game> {
    const games = this.games.games;
    for (let i = 0; i < games.length; i++) {
      if (games[i].id === game.id) {
        games[i] = game;
        return new Observable(subscriber => {
          subscriber.next(game);
        });
      }
    }
    return new Observable();
  }
}
