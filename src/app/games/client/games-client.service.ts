import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, OperatorFunction } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import Games from '@app/games/models/games.model';
import Game from '@app/games/models/game.model';
import Genre from '@app/games/models/genre.model';
import System from '@app/games/models/system.model';
import GameClient from '@app/games/client/game.client';
import Notifications from '@app/notifications/notifications';

const routes = {
  games: () => `http://127.0.0.1:8081/games`,
  genre: () => `http://127.0.0.1:8081/genre`,
  systems: () => `http://127.0.0.1:8081/systems`
};

@Injectable({
  providedIn: 'root'
})
export class GamesClientService implements GameClient {
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private httpClient: HttpClient,
    @Inject('Notifications') @Optional() private notifications: Notifications
  ) {}

  getAll(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Games> {
    return this.httpClient.get<Games>(routes.games()).pipe(
      map((body: any) => body),
      this.handleCustomError(errorHandler)
    );
  }

  delete(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<any> {
    return this.httpClient.delete<null>(routes.games() + '/' + game.id).pipe(
      map((body: any) => body),
      this.handleCustomError(errorHandler)
    );
  }

  getGenres(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Genre[]> {
    return this.httpClient.get<Genre[]>(routes.genre()).pipe(
      map((body: any) => body),
      this.handleCustomError(errorHandler)
    );
  }

  getSystems(errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<System[]> {
    return this.httpClient.get<System[]>(routes.systems()).pipe(
      map((body: any) => body),
      catchError(this.handleError())
    );
  }

  create(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game> {
    return this.httpClient.post<Game>(routes.games(), game, this.httpOptions).pipe(
      tap((createdGame: Game) => console.log(`created game w/ id=${createdGame.id}`)),
      this.handleCustomError(errorHandler)
    );
  }

  update(game: Game, errorHandler?: (error: HttpErrorResponse) => Observable<any>): Observable<Game> {
    return this.httpClient.post<Game>(routes.games() + '/' + game.id, game, this.httpOptions).pipe(
      tap((createdGame: Game) => console.log(`updated game w/ id=${createdGame.id}`)),
      this.handleCustomError(errorHandler)
    );
  }

  private handleCustomError(errorHandler?: (error: HttpErrorResponse) => Observable<any>): OperatorFunction<Game, any> {
    return catchError(errorHandler === undefined ? this.handleError() : errorHandler);
  }

  private handleError(): (error: HttpErrorResponse) => Observable<any> {
    return (error: HttpErrorResponse): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      // TODO enhance logging to be more intelligent in how errors are reported
      if (this.notifications) {
        if ('message' in error.error) {
          this.notifications.notify(error.error.message, 'Error');
        } else {
          this.notifications.notify(error.message, 'Error');
        }
      }
      // Let the app keep running by returning an empty result.
      return of();
    };
  }
}
