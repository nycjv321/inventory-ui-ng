import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { ListingGridRoutingModule } from './listing-grid-routing.module';
import { GameListingGridComponent } from './game-listing-grid.component';
import { GameCardComponent } from '@app/games/listing-grid/game-card/game-card.component';
import { DeleteGameConfirmationComponent } from '@app/games/delete-confirmation/delete-game-confirmation.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    ListingGridRoutingModule
  ],
  entryComponents: [DeleteGameConfirmationComponent],
  declarations: [GameListingGridComponent, DeleteGameConfirmationComponent, GameCardComponent]
})
export class GameListingGridModule {}
