import { Component, Inject, Input, OnInit } from '@angular/core';
import { Game } from '@app/games/models/game.model';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DeleteGameConfirmationComponent } from '@app/games/delete-confirmation/delete-game-confirmation.component';
import { printLine } from 'tslint/lib/verify/lines';
import { GamesClientService } from '@app/games/client/games-client.service';
import Notifications from '@app/notifications/notifications';
import GameClient from '@app/games/client/game.client';

@Component({
  selector: 'game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {
  @Input()
  game: Game;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    @Inject('GameClient') private gameService: GameClient,
    @Inject('Notifications') private notifications: Notifications
  ) {}

  ngOnInit() {}

  edit() {
    this.router.navigate(['add-game'], { queryParams: { game: JSON.stringify(this.game) } });
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteGameConfirmationComponent, {
      width: '250px',
      data: { title: this.game.title, id: this.game.id }
    });

    dialogRef.afterClosed().subscribe(del => {
      if (del) {
        this.gameService.delete(this.game).subscribe(response => {
          const title = this.game.title;
          this.notifications.notify('Game (' + title + ') was deleted!', 'Success!');
          this.router.navigate(['/']);
          this.game = undefined;
        });
      }
    });
  }
}
