import { Component, Inject, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import Game from '@app/games/models/game.model';
import Games from '@app/games/models/games.model';
import GameClient from '@app/games/client/game.client';
import Notifications from '@app/notifications/notifications';

@Component({
  selector: 'app-home',
  templateUrl: './game-listing-grid.component.html',
  styleUrls: ['./game-listing-grid.component.scss']
})
export class GameListingGridComponent implements OnInit {
  games: Game[] | undefined;
  isLoading = false;

  constructor(
    @Inject('GameClient') private gameService: GameClient,
    @Inject('Notifications') private notifications: Notifications
  ) {}

  ngOnInit() {
    this.isLoading = true;
    this.gameService
      .getAll()
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((games: Games) => {
        this.games = games.games;
      });
  }
}
