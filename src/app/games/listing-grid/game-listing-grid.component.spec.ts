import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { GameListingGridComponent } from './game-listing-grid.component';
import { GamesClientService } from '../client/games-client.service';
import { GameClientProvider } from '@app/games/providers/games.test.provider';
import { GameCardComponent } from '@app/games/listing-grid/game-card/game-card.component';
import { NotificationsProvider } from '@app/notifications/notifications.test.provider';

describe('GameListingGridComponent', () => {
  let component: GameListingGridComponent;
  let fixture: ComponentFixture<GameListingGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FlexLayoutModule,
        MaterialModule,
        CoreModule,
        SharedModule,
        HttpClientTestingModule
      ],
      declarations: [GameListingGridComponent, GameCardComponent],
      providers: [GameClientProvider, NotificationsProvider]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameListingGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('should create', () => {
    expect(component).toBeTruthy();
  });
});
