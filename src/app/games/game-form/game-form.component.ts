import { Component, Inject, Input, OnInit } from '@angular/core';

import { Game, createEmptyGame, formattedDate } from '@app/games/models/game.model';
import System from '@app/games/models/system.model';
import Genre from '@app/games/models/genre.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Notifications from '@app/notifications/notifications';
import GameClient from '@app/games/client/game.client';

// TODO switch to: https://angular.io/guide/dynamic-form
@Component({
  selector: 'game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.scss']
})
export class GameFormComponent implements OnInit {
  @Input()
  game: Game | undefined;
  releaseDate: Date | undefined;

  systems: System[] | undefined;
  genres: Genre[] | undefined;

  constructor(
    @Inject('GameClient') private gameService: GameClient,
    @Inject('Notifications') private notifications: Notifications,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.game = createEmptyGame();

    this.route.queryParams.subscribe((params: Params) => {
      if (params.game !== undefined) {
        const game_json = JSON.parse(params.game);
        this.game.title = game_json.title;
        this.game.description = game_json.description;
        this.releaseDate = new Date(game_json.release_date);
        this.game.release_date = game_json.release_date;
        this.game.system = game_json.system;
        this.game.genre = game_json.genre;
        this.game.id = game_json.id;
      }
    });

    this.gameService.getSystems().subscribe((systems: System[]) => {
      this.systems = systems;
    });

    this.gameService.getGenres().subscribe((genre: Genre[]) => {
      this.genres = genre;
    });
  }

  onSubmit() {
    this.game.release_date = formattedDate(this.releaseDate);
    if (this.game.id !== undefined && this.game.id >= 0) {
      this.gameService.update(this.game).subscribe((game: Game) => {
        this.notifications.success('Game (' + game.title + ') updated!');
        this.router.navigate(['/']);
        this.game = undefined;
      });
    } else {
      this.gameService.create(this.game).subscribe((game: Game) => {
        this.notifications.success('Game (' + game.title + ') created!');
        this.router.navigate(['/']);
        this.game = undefined;
      });
    }
  }
}
