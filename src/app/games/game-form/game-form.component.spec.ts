import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { GameClientProvider } from '@app/games/providers/games.test.provider';
import { GameFormComponent } from '@app/games/game-form/game-form.component';
import { FormsModule } from '@angular/forms';
import { NotificationsProvider } from '@app/notifications/notifications.test.provider';
import { RouterTestingModule } from '@angular/router/testing';

describe('GameListingGridComponent', () => {
  let component: GameFormComponent;
  let fixture: ComponentFixture<GameFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FlexLayoutModule,
        MaterialModule,
        CoreModule,
        MaterialModule,
        FormsModule,
        SharedModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [GameFormComponent],
      providers: [GameClientProvider, NotificationsProvider]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('error when fields are omitted', () => {
    spyOn(console, 'error');
    expect(component).toBeTruthy();
    expect(console.error).toHaveBeenCalledTimes(0);
    fixture.nativeElement.querySelector('button[type="submit"]').click();

    // TODO this test is essentially coding for a bug. The form should instead
    //  properly report the error to the user. Will address in a future commit

    expect(console.error).toHaveBeenCalled();
  });
});
