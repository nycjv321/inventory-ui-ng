import { GamesClientService } from '@app/games/client/games-client.service';

export let GameClientProvider = [{ provide: 'GameClient', useClass: GamesClientService }];
