import MockGameClient from '@app/games/client/mock-games-client.service';

export let GameClientProvider = [{ provide: 'GameClient', useValue: new MockGameClient() }];
