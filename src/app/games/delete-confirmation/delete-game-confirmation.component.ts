import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'delete-game-confirmation',
  templateUrl: 'delete-game-confirmation.component.html'
})
export class DeleteGameConfirmationComponent {
  constructor(
    public dialogRef: MatDialogRef<DeleteGameConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: string; id: number }
  ) {}

  no(): void {
    this.dialogRef.close(false);
  }

  yes(): void {
    this.dialogRef.close(true);
  }
}
