import { Navigation } from '../pages/navigation/navigation.po';
import { AssertionError } from 'assert';
import { AddGameForm } from '../pages/add-game-form.po';
import { GameListingGrid } from '../pages/game-listing-grid.po';

export class PageFactory {
  static async createPage(type: 'navigation'): Promise<Navigation>;
  static async createPage(type: 'view all'): Promise<GameListingGrid>;
  static async createPage(type: 'add game'): Promise<AddGameForm>;

  static async createPage(type: string) {
    let page;
    switch (type.toLowerCase()) {
      case 'navigation':
        page = new Navigation();
        break;
      case 'view all':
        page = new GameListingGrid();
        break;
      case 'add game':
        page = new AddGameForm();
        break;
      default:
        throw new AssertionError({ message: `Page type ${type} is not currently supported.` });
    }

    await page.isDisplayed();
    return page;
  }
}
