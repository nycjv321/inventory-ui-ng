import { by, ElementFinder } from 'protractor';

export class GameCard {
  private root: ElementFinder;

  private titleElem: ElementFinder;
  private releaseDateElem: ElementFinder;
  private descriptionElem: ElementFinder;

  private editBtn: ElementFinder;
  private deleteBtn: ElementFinder;

  constructor(root: ElementFinder) {
    this.root = root;

    this.titleElem = this.root.element(by.className('game-title'));
    this.releaseDateElem = this.root.element(by.className('game-release-date'));
    this.descriptionElem = this.root.element(by.className('game-description'));

    this.editBtn = this.root.element(by.name('edit'));
    this.deleteBtn = this.root.element(by.name('delete'));
  }

  async title() {
    return await this.titleElem.getText();
  }

  async releaseDate() {
    return await this.releaseDateElem.getText();
  }

  async description() {
    return await this.descriptionElem.getText();
  }

  async edit() {
    await this.editBtn.click();
  }

  async delete() {
    await this.deleteBtn.click();
  }
}
