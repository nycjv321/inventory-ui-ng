import { BeforeAll } from 'cucumber';
import { browser } from 'protractor';

BeforeAll(async () => {
  await browser.driver.get(browser.baseUrl);
});
