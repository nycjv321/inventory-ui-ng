// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  SELENIUM_PROMISE_MANAGER: false,
  allScriptsTimeout: 11000,
  capabilities: {
    browserName: process.env.PROTRACTOR_BROWSER || 'chrome',
    chromeOptions: {
      binary: process.env.PROTRACTOR_CHROME_BIN || undefined,
      args: process.env.PROTRACTOR_CHROME_ARGS ? JSON.parse(process.env.PROTRACTOR_CHROME_ARGS) : ['lang=en-US'],
      prefs: {
        intl: { accept_languages: 'en-US' }
      }
    }
  },
  framework: 'custom',

  frameworkPath: require.resolve('protractor-cucumber-framework'), // path relative to the current config file
  specs: [
    './features/*.feature' // Specs here are the cucumber feature files
  ],

  // Only works with Chrome and Firefox
  directConnect: true,
  baseUrl: 'http://localhost:4200/',

  // cucumber command line options
  cucumberOpts: {
    require: ['./features/step_definitions/**/*.steps.ts', './support/*.ts'], // require step definition files before executing features
    tags: [], // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true, // <boolean> fail if there are any undefined or pending step_definitions
    // see https://github.com/cucumber/cucumber-js/issues/1152
    format: [], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    'dry-run': false, // <boolean> invoke formatters without executing step_definitions
    compiler: [] // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  },

  // framework: 'jasmine2',
  // jasmineNodeOpts: {
  //   showColors: true,
  //   defaultTimeoutInterval: 30000,
  //   print: function() {}
  // },

  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });

    browser.waitForAngularEnabled(false);

    //// Better console spec reporter
    // jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
