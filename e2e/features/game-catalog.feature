Feature: Game Catalog
  In order to manage my game catalog
  As a gamer
  I want be able to manage and audit my game collection

 Scenario: Add Games
    Given I am on the "add game" page
    When I add a new game named "Splatoon" with description "Fun with paint!"
    Then the game "Splatoon" should be in the listing grid
#    And a notification should display acknowledging a successful operation

