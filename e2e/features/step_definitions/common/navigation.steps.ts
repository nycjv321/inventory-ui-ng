import { Given } from 'cucumber';
import { PageFactory } from '../../../utils/page-factory';

Given('I am on the {string} page', async page => {
  const nav = await PageFactory.createPage('navigation');
  await nav.navigateTo(page);
  await PageFactory.createPage(page);
});
