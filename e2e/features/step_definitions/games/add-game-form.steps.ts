import { PageFactory } from '../../../utils/page-factory';
import { When } from 'cucumber';

When('I add a new game named {string} with description {string}', async (name: string, desc: string) => {
  const game = {
    id: -1,
    title: name,
    description: desc,
    release_date: '12/01/1999',
    system: 'Nintendo Switch',
    genre: 'Platform'
  };

  const addGameForm = await PageFactory.createPage('add game');
  await addGameForm.addGame(game);
  await addGameForm.submit();
});
