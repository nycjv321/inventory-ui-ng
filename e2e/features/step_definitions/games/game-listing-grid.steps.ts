import { Then } from 'cucumber';
import { PageFactory } from '../../../utils/page-factory';

const expect = require('expect');

Then('the game {string} should be in the listing grid', async title => {
  const gameListingGrid = await PageFactory.createPage('view all');
  expect(await gameListingGrid.game(title)).not.toBeNull();
});
