import { browser, element, by, ExpectedConditions as until } from 'protractor';
import Game from '@app/games/models/game.model';
import { BasePage } from './base.po';

export class AddGameForm extends BasePage {
  private addGameForm = element(by.className('add-game-form'));

  // Add Game Form Fields
  private titleField = element(by.name('title'));
  private systemField = element(by.name('system'));
  private releaseDateField = element(by.name('release_date'));
  private genreField = element(by.name('genre'));
  private descriptionField = element(by.name('description'));

  // Page Controls
  private submitButton = element(by.css('button[type="submit"]'));

  async isDisplayed(): Promise<boolean> {
    try {
      await browser.wait(until.visibilityOf(this.addGameForm), 5000, 'Add Game Form has not loaded');
      return true;
    } catch (err) {
      return false;
    }
  }

  async addGame(game: Game) {
    await this.titleField.sendKeys(game.title);
    await this.releaseDateField.sendKeys(game.release_date);
    await this.genreField.sendKeys(game.genre);
    await this.descriptionField.sendKeys(game.description);
    await this.selectSystem(game.system);
  }

  async submit() {
    await this.submitButton.click();
  }

  private async selectSystem(system: string) {
    await this.systemField.sendKeys(system);
    await element(by.xpath(`//mat-option/span[text()=' ${system} ']`)).click();
  }
}
