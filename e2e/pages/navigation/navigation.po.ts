import { browser, element, by, ExpectedConditions as until } from 'protractor';
import { BasePage } from '../base.po';
import { AssertionError } from 'assert';

export class Navigation extends BasePage {
  private navMenu = element(by.tagName('nav'));
  private navMenuItems = {
    viewAll: element(by.id('nav-view-all')),
    addGame: element(by.id('nav-add-game')),
    about: element(by.id('nav-about'))
  };

  async isDisplayed(): Promise<boolean> {
    try {
      await browser.wait(until.visibilityOf(this.navMenu), 5000);
      return true;
    } catch (err) {
      return false;
    }
  }

  async navigateTo(page: string) {
    switch (page.toLowerCase()) {
      case 'view all':
        await this.navMenuItems.viewAll.click();
        break;
      case 'add game':
        await this.navMenuItems.addGame.click();
        break;
      case 'about':
        await this.navMenuItems.addGame.click();
        break;
      default:
        throw new AssertionError({ message: `Cannot navigate to ${page} because it is not a valid option` });
    }
  }

  // async navigateAndSetLanguage() {
  //   // Forces default language
  //   await this.navigateTo();
  //   await browser.executeScript(() => localStorage.setItem('language', 'en-US'));
  // }
}
