export abstract class BasePage {
  abstract isDisplayed(): Promise<boolean>;
}
