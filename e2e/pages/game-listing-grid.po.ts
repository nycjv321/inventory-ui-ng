import { browser, element, by, ExpectedConditions as until } from 'protractor';
import { BasePage } from './base.po';
import { GameCard } from '../components/game-card.po';

export class GameListingGrid extends BasePage {
  private listingGrid = element(by.css('.mat-grid-list'));
  private cards = element.all(by.className('card'));

  async isDisplayed(): Promise<boolean> {
    try {
      await browser.wait(until.visibilityOf(this.listingGrid), 5000, 'Game List has not loaded');
      return true;
    } catch (err) {
      return false;
    }
  }

  async games() {
    return await this.cards.map((card, idx) => {
      return new GameCard(card);
    });
  }

  async game(name: string) {
    const gameCard = await this.cards
      .filter((card, idx) => {
        return card.element(by.className('game-title')).then(title => {
          return title === name;
        });
      })
      .first();

    return new GameCard(gameCard);
  }
}
